package bankingDemo;

import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;

public class BankProgram {
	private HashMap<Integer, Integer> accounts = new HashMap<>();
	private double rate = 1;
	private int nextacct = 0;
	private int current = -1;
	private Scanner scanner;
	private boolean done = false;

	public static void main(String[] args) {
		BankProgram program = new BankProgram();
		program.run();
	}

	private void run() {
		scanner = new Scanner(System.in);
		while (!done) {
			System.out.println("Enter command (0=quit, 1=new, 2=select, 3=deposit, 4=loan, 5=show, 6=interest): ");
			int cmd = scanner.nextInt();
			processCommand(cmd);
		}
		scanner.close();
	}

	private void processCommand(int cmd) {
		if (cmd == 0) quit();
		else if (cmd == 1) newAccount();
		else if (cmd == 2) select();
		else if (cmd == 3) deposit();
		else if (cmd == 4) authorizeLoan();
		else if (cmd == 5) showAll();
		else if (cmd == 6) addInterest();
		else
			System.out.println("illegal command");
	}

	private void addInterest() {
		Set<Integer> keys = accounts.keySet();
		for (int key : keys) {
			int balance = accounts.get(key);
			int newBalance = (int) (balance * (1 + rate));
			accounts.put(key, newBalance);
		}
	}

	private void showAll() {
		Set<Integer> keys = accounts.keySet();
		System.out.printf("O banco tem [%d] contas\n", keys.size());

		for (int key : keys) {
			System.out.printf("Conta [%d] tem saldo [%s]\n", key, accounts.get(key));
		}
	}

	private void authorizeLoan() {
		System.out.println("Insira o valor do empréstimo: ");
		int loan = scanner.nextInt();
		int balance = accounts.get(current);
		if (balance >= loan/2) {
			System.out.println("Empréstimo aprovado");
		} else {
			System.out.println("Empréstimo negado");
		}
	}

	private void deposit() {
		System.out.println("Insira o valor a ser depositado: ");
		int amount = scanner.nextInt();
		int balance = accounts.get(current);
		accounts.put(current, balance + amount);
		System.out.printf("Depositado o valor de [%d] na conta [%d]\n", amount, current);
	}

	private void select() {
		System.out.println("Digite o número da conta: ");
		current = scanner.nextInt();
		int balance = accounts.get(current);
		System.out.printf("O saldo da conta [%d] é [%d]\n", current, balance);
	}

	private void newAccount() {
		current = nextacct++;
		accounts.put(current, 0);
		System.out.printf("Conta criada com id [%d]\n", current);
	}

	private void quit() {
		done = true;
		System.out.println("Goodbye!");
	}

}